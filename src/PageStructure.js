import PageConstant from './PageConstant';

const languageCode = ['en', 'de', 'es', 'pt', 'zh', 'ru', 'fr'];

/**
 * Create pathname with language code
 *
 * @param {String} pathname
 * @returns {Array} []
 */
const createPathNameWithLanguageCode = pathname => {
  if (typeof pathname !== 'string') {
    throw new Error('pathname must be a string');
  }

  let i = 0;
  const pathnames = [];

  for (i; i < languageCode.length; i += 1) {
    pathnames.push(languageCode[i] + '/' + pathname);
  }

  return pathnames;
};

export const pagesObject = {
  home: {
    page: PageConstant.HOME,
    deepLink: '/home',
    isAttributes: false,
    href: 'https://www.getabstract.com'
  },
  explore: {
    page: PageConstant.EXPLORE,
    deepLink: '/explore',
    isAttributes: false,
    pathnames: [
      ...createPathNameWithLanguageCode('explore'),
      'explore',
      'AbstractList.do',
      'summaries',
      'en/summaries/books-lib/',
      'de/zusammenfassungen',
      'es/resúmenes',
      'pt/resumos',
      'fr/resumes'
    ]
  },
  search: {
    page: PageConstant.SEARCH,
    deepLink: '/search',
    isAttributes: false,
    pathnames: [...createPathNameWithLanguageCode('search'), 'search']
  },
  mylibrary: {
    page: PageConstant.MYLIBRARY,
    deepLink: '/mylibrary',
    isAttributes: false,
    pathnames: ['mysummaries', 'mylibrary']
  },
  profile: {
    page: PageConstant.PROFILE,
    deepLink: '/profile',
    isAttributes: false,
    pathnames: ['myaccount']
  },
  settings: {
    page: PageConstant.SETTINGS,
    deepLink: '/settings',
    isAttributes: false,
    pathnames: ['settings']
  },
  settingsNotifications: {
    page: PageConstant.SETTINGS_NOTIFICATIONS,
    deepLink: '/settings/notifications',
    isAttributes: false,
    pathnames: ['settings/notifications']
  },
  summary: {
    page: PageConstant.SUMMARY,
    deepLink: '/summaries',
    isAttributes: true,
    pathnames: [
      '/ShowAbstract.do',
      '/summary',
      '/de/summary',
      '/en/summary',
      '/de/zusammenfassung',
      '/es/resumen',
      '/pt/resumo',
      '/fr/resume',
      '/ru/Краткое-изложение',
      '/zh/书摘/销售与市场营销'
    ],
    patterns: '/summaries/{dataId}'
  },
  summarySource: {
    page: PageConstant.SUMMARY_SOURCE,
    deepLink: '/summary-sources',
    patterns: '/summary-sources/{dataId}'
  },
  channel: {
    page: PageConstant.CHANNEL,
    deepLink: '/channels',
    isAttributes: true,
    pathnames: [...createPathNameWithLanguageCode('channels'), '/channels']
  },
  portlet: {
    page: PageConstant.PORTLET,
    deepLink: '/portlets',
    isAttributes: true,
    pathnames: [...createPathNameWithLanguageCode('portlets'), '/portlets']
  },
  audioPlayer: {
    page: PageConstant.AUDIOPLAYER,
    deepLink: '/audioplayer'
  },
  playlist: {
    page: PageConstant.PLAYLIST,
    deepLink: '/playlist'
  },
  playlistHistory: {
    page: PageConstant.PLAYLIST_HISTORY,
    deepLink: '/playlist/history'
  },
  upgrade: {
    page: PageConstant.UPGRADE,
    deepLink: '/upgrade'
  },
  subscription: {
    page: PageConstant.SUBSCRIPTION,
    deepLink: '/subscriptions'
  }
};
