import UrlParse from 'url-parse';

import { pagesObject } from './PageStructure';

const pagesKeys = Object.keys(pagesObject);
const pagesValues = Object.values(pagesObject);
const DEEPLINK = 'DEEPLINK';

/**
 * Replace all backsplashes
 * @example /en/explore -> enexplore
 * @param {String} str
 */
const replaceBackSplashes = (str = '') => str.replace(/[/]/g, '');

const getIdFromLink = link => {
  let id = null;

  if (link) {
    const uriArray = link.split('/');

    if (uriArray && uriArray.length) {
      id = uriArray.pop();
      id = parseInt(`${id}`.match(/\d+/), 10);
    }
  }

  return id;
};

/**
 * Get deepLink
 * @param {Object} { href, pathname }
 * @returns {String} deepLink
 */
export const handleUniversalLink = url => {
  if (typeof url !== 'string') {
    throw new Error('url must be a string');
  }

  let page = null;
  const { href, pathname, query } = new UrlParse(url, true);
  const pathnameWithoutBackSplashes = replaceBackSplashes(pathname);
  let { dataId } = query;
  let i = 0;

  dataId = parseInt(dataId);
  for (i; i < pagesKeys.length; i += 1) {
    const pageData = pagesValues[i];

    // home screen
    if (pathname === '' && href === pageData.href) {
      page = pageData.page;
      break;
    }
    if (pageData.pathnames) {
      let j = 0;

      for (j; j < pageData.pathnames.length; j += 1) {
        const elementWithoutBackSplashes = replaceBackSplashes(
          pageData.pathnames[j]
        );

        if (pathnameWithoutBackSplashes === elementWithoutBackSplashes) {
          page = pageData.page;
          break;
        } else if (pageData.isAttributes) {
          if (!dataId) {
            const index = pathname.indexOf(pageData.pathnames[j]);

            if (index > -1) {
              dataId = getIdFromLink(pathname);
              page = pageData.page;
              break;
            }
          }
        }
      }
      if (page) {
        break;
      }
    }
  }

  return {
    href,
    query,
    dataId,
    page
  };
};

/**
 * Get page and dataId from deepLink
 * @param {String} deepLink
 * @returns {Object} {dataId, page}
 */
export const handleDeepLink = deepLink => {
  if (typeof deepLink !== 'string') {
    throw new Error('deepLink must be a string');
  }

  const dataId = getIdFromLink(deepLink);
  let page = null;
  let index = -1;
  let i = 0;

  for (i; i < pagesKeys.length; i += 1) {
    const pageData = pagesValues[i];

    if (dataId) {
      index = deepLink.indexOf(pageData.deepLink);
    }
    if (deepLink === pageData.deepLink || index > -1) {
      page = pageData.page;
    }
  }

  return {
    dataId,
    page
  };
};

/**
 * Either a Deep Link or a link to an external URI. This depends on the actionType
 * @param {Object} dataStructure
 * @param {String} dataStructure.uri - Either a Deep Link or a link to an external URI. This depends on the actionType
 * @param {String} dataStructure.actionType - Defines the type of the action. It can either be an DEEPLINK which opens a screen within the app,
 * or a EXTERNAL link which opens in the browser
 * Types: DEEPLINK, EXTERNAL
 */
export const handleUri = ({ uri, actionType }) => {
  if (actionType === DEEPLINK) {
    return handleDeepLink(uri);
  }

  return handleUniversalLink(uri);
};
