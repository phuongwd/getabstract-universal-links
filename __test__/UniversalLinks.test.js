import { handleUniversalLink } from '../src';
import PageConstant from '../src/PageConstant';

const expectDeepLinks = (links, expDeepLink, expDataId) => {
  for (let i = 0; i < links.length; i += 1) {
    const href = links[i];
    const { page, dataId } = handleUniversalLink(href);
    let msg = 'from ' + href;

    if (dataId) {
      msg = 'and dataId=' + dataId + ' ' + msg;
    }
    test(msg, () => {
      expect(page).toBe(expDeepLink);
      if (dataId) {
        expect(dataId).toBe(expDataId);
      }
    });
  }
};

describe('Universal Links', () => {
  describe('return page ' + PageConstant.HOME, () => {
    test('from https://www.getabstract.com', () => {
      const { page } = handleUniversalLink('https://www.getabstract.com');

      expect(page).toBe(PageConstant.HOME);
    });
  });

  describe('Open "getabstract" URLs in browser', () => {
    const url =
      'https://www.getabstract.com/CompetencyOutput.do?action=coreCompetency&competencyId=112158';

    test('from ' + url, () => {
      const { page, href } = handleUniversalLink(url);

      expect(page).toBe(null);
      expect(href).toBe(url);
    });
  });

  describe('return page ' + PageConstant.EXPLORE, () => {
    const explores = [
      'https://www.getabstract.com/explore',
      'https://www.getabstract.com/en/explore',
      'https://www.getabstract.com/de/explore',
      'https://www.getabstract.com/es/explore',
      'https://www.getabstract.com/pt/explore',
      'https://www.getabstract.com/zh/explore',
      'https://www.getabstract.com/ru/explore',
      'https://www.getabstract.com/fr/explore',
      'https://www.getabstract.com/AbstractList.do?action=abstractList',
      'https://www.getabstract.com/summaries',
      'https://www.getabstract.com/en/summaries/books-lib/',
      'https://www.getabstract.com/en/summaries/books-lib',
      'https://www.getabstract.com/de/zusammenfassungen',
      'https://www.getabstract.com/es/resúmenes',
      'https://www.getabstract.com/pt/resumos',
      'https://www.getabstract.com/fr/resumes'
    ];

    expectDeepLinks(explores, PageConstant.EXPLORE);
  });

  describe('return page ' + PageConstant.SEARCH, () => {
    test('from https://www.getabstract.com/search', () => {
      const { page } = handleUniversalLink(
        'https://www.getabstract.com/search'
      );

      expect(page).toBe(PageConstant.SEARCH);
    });
  });

  describe('return page ' + PageConstant.MYLIBRARY, () => {
    const mylibrary = [
      'https://www.getabstract.com/mylibrary',
      'https://www.getabstract.com/mysummaries'
    ];

    expectDeepLinks(mylibrary, PageConstant.MYLIBRARY);
  });

  describe('return page ' + PageConstant.PROFILE, () => {
    test('from https://www.getabstract.com/myaccount', () => {
      const { page } = handleUniversalLink(
        'https://www.getabstract.com/myaccount'
      );

      expect(page).toBe(PageConstant.PROFILE);
    });
  });

  describe('return page ' + PageConstant.SETTINGS, () => {
    test('from https://www.getabstract.com/settings', () => {
      const { page } = handleUniversalLink(
        'https://www.getabstract.com/settings'
      );

      expect(page).toBe(PageConstant.SETTINGS);
    });
  });

  describe('return page ' + PageConstant.SETTINGS_NOTIFICATIONS, () => {
    test('from https://www.getabstract.com/settings/notifications', () => {
      const { page } = handleUniversalLink(
        'https://www.getabstract.com/settings/notifications'
      );

      expect(page).toBe(PageConstant.SETTINGS_NOTIFICATIONS);
    });
  });

  describe('return page ' + PageConstant.SUMMARY, () => {
    const summaries = [
      'https://www.getabstract.com/ShowAbstract.do?dataId=20210',
      'https://www.getabstract.com/ShowAbstract.do?dataId=20210&u=Deacero&st=FLYER&si=4134&si2=123',
      'https://www.getabstract.com/summary/20210',
      'https://www.getabstract.com/de/summary/20210',
      'https://www.getabstract.com/en/summary/sales-and-marketing/hustle/20210',
      'https://www.getabstract.com/de/zusammenfassung/marketing-und-verkauf/hustle/20210',
      'https://www.getabstract.com/es/resumen/ventas-y-marketing/hustle/20210',
      'https://www.getabstract.com/pt/resumo/vendas-e-marketing/hustle/20210',
      'https://www.getabstract.com/fr/resume/ventes-et-marketing/hustle/20210',
      'https://www.getabstract.com/ru/Краткое-изложение/продажи-и-маркетинг/hustle/20210',
      'https://www.getabstract.com/zh/书摘/销售与市场营销/hustle/20210',
      'https://www.getabstract.com/new-pattern/summary/new-patternnew-patternnew-patternnew-patternnew-patternnew-patternnew-pattern/20210',
      'https://www.getabsttract.com/summary/hustle/20210'
    ];

    expectDeepLinks(summaries, PageConstant.SUMMARY, 20210);
  });

  describe(PageConstant.SUMMARY_SOURCE, () => {
    const url =
      'https://www.getabstract.com/BuyBook.do?dataId=20210&s=web&outputType=jsp';

    test('return href ' + url + ' from ' + url, () => {
      const { page, href } = handleUniversalLink(url);

      expect(page).toBe(null);
      expect(href).toBe(url);
    });
  });

  describe('return page ' + PageConstant.CHANNEL, () => {
    const channels = [
      'https://www.getabstract.com/channels/1050',
      'https://www.getabstract.com/en/channels/career/1050'
    ];

    expectDeepLinks(channels, PageConstant.CHANNEL, 1050);
  });

  describe('return page ' + PageConstant.PORTLET, () => {
    const portlets = [
      'https://www.getabstract.com/portlets/489',
      'https://www.getabstract.com/de/portlets/489'
    ];

    expectDeepLinks(portlets, PageConstant.PORTLET, 489);
  });

  test('#invalid: href must be String', () => {
    try {
      handleUniversalLink(123456);
    } catch (error) {
      expect(error instanceof Error).toBeTruthy();
    }
  });
});
