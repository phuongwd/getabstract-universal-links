import { handleDeepLink } from '../src';
import PageConstant from '../src/PageConstant';

describe('Deep Links', () => {
  describe('return page ' + PageConstant.HOME, () => {
    test('from /home', () => {
      const { page } = handleDeepLink('/home');

      expect(page).toBe(PageConstant.HOME);
    });
  });

  describe('return page ' + PageConstant.EXPLORE, () => {
    test('from /explore', () => {
      const { page } = handleDeepLink('/explore');

      expect(page).toBe(PageConstant.EXPLORE);
    });
  });

  describe('return page ' + PageConstant.SEARCH, () => {
    test('from /search', () => {
      const { page } = handleDeepLink('/search');

      expect(page).toBe(PageConstant.SEARCH);
    });
  });

  describe('return page ' + PageConstant.MYLIBRARY, () => {
    test('from /mylibrary', () => {
      const { page } = handleDeepLink('/mylibrary');

      expect(page).toBe(PageConstant.MYLIBRARY);
    });
  });

  describe('return page ' + PageConstant.PROFILE, () => {
    test('from /profile', () => {
      const { page } = handleDeepLink('/profile');

      expect(page).toBe(PageConstant.PROFILE);
    });
  });

  describe('return page ' + PageConstant.SETTINGS, () => {
    test('from /settings', () => {
      const { page } = handleDeepLink('/settings');

      expect(page).toBe(PageConstant.SETTINGS);
    });
  });

  describe('return page ' + PageConstant.SETTINGS_NOTIFICATIONS, () => {
    test('from /settings/notifications', () => {
      const { page } = handleDeepLink('/settings/notifications');

      expect(page).toBe(PageConstant.SETTINGS_NOTIFICATIONS);
    });
  });

  describe('return page ' + PageConstant.SUMMARY + ' and dataId=20210', () => {
    test('from /summaries/20210', () => {
      const { page, dataId } = handleDeepLink('/summaries/20210');

      expect(page).toBe(PageConstant.SUMMARY);
      expect(dataId).toBe(20210);
    });
  });

  describe(
    'return page ' + PageConstant.SUMMARY_SOURCE + ' and dataId=20210',
    () => {
      test('from /summary-sources/20210', () => {
        const { page, dataId } = handleDeepLink('/summary-sources/20210');

        expect(page).toBe(PageConstant.SUMMARY_SOURCE);
        expect(dataId).toBe(20210);
      });
    }
  );

  describe('return page ' + PageConstant.CHANNEL + ' and dataId=1050', () => {
    test('from /channels/1050', () => {
      const { page, dataId } = handleDeepLink('/channels/1050');

      expect(page).toBe(PageConstant.CHANNEL);
      expect(dataId).toBe(1050);
    });
  });

  describe('return page ' + PageConstant.PORTLET + ' and dataId=489', () => {
    test('from /portlets/489', () => {
      const { page, dataId } = handleDeepLink('/portlets/489');

      expect(page).toBe(PageConstant.PORTLET);
      expect(dataId).toBe(489);
    });
  });

  describe('return page ' + PageConstant.AUDIOPLAYER, () => {
    test('from /audioplayer', () => {
      const { page } = handleDeepLink('/audioplayer');

      expect(page).toBe(PageConstant.AUDIOPLAYER);
    });
  });

  describe('return page ' + PageConstant.PLAYLIST, () => {
    test('from /playlist', () => {
      const { page } = handleDeepLink('/playlist');

      expect(page).toBe(PageConstant.PLAYLIST);
    });
  });

  describe('return page ' + PageConstant.PLAYLIST_HISTORY, () => {
    test('from /playlist/history', () => {
      const { page } = handleDeepLink('/playlist/history');

      expect(page).toBe(PageConstant.PLAYLIST_HISTORY);
    });
  });

  describe('return page ' + PageConstant.UPGRADE, () => {
    test('from /upgrade', () => {
      const { page } = handleDeepLink('/upgrade');

      expect(page).toBe(PageConstant.UPGRADE);
    });
  });

  describe('return page ' + PageConstant.SUBSCRIPTION, () => {
    test('from /subscriptions', () => {
      const { page } = handleDeepLink('/subscriptions');

      expect(page).toBe(PageConstant.SUBSCRIPTION);
    });
  });

  test('#invalid: deepLink must be String', () => {
    try {
      handleDeepLink(123456);
    } catch (error) {
      expect(error instanceof Error).toBeTruthy();
    }
  });
});
