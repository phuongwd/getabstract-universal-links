import { handleUri } from '../src';
import PageConstant from '../src/PageConstant';

describe('handleUri', () => {
  test('return page ' + PageConstant.HOME + ' from /home', () => {
    const { page } = handleUri({ uri: '/home', actionType: 'DEEPLINK' });

    expect(page).toBe(PageConstant.HOME);
  });
  test('return page ' + PageConstant.EXPLORE + ' from /explore', () => {
    const { page } = handleUri({ uri: '/explore', actionType: 'DEEPLINK' });

    expect(page).toBe(PageConstant.EXPLORE);
  });

  test('return page ' + PageConstant.SEARCH + ' from /search', () => {
    const { page } = handleUri({ uri: '/search', actionType: 'DEEPLINK' });

    expect(page).toBe(PageConstant.SEARCH);
  });

  test('return page ' + PageConstant.MYLIBRARY + ' from /mylibrary', () => {
    const { page } = handleUri({ uri: '/mylibrary', actionType: 'DEEPLINK' });

    expect(page).toBe(PageConstant.MYLIBRARY);
  });

  test('return page ' + PageConstant.PROFILE + ' from /profile', () => {
    const { page } = handleUri({ uri: '/profile', actionType: 'DEEPLINK' });

    expect(page).toBe(PageConstant.PROFILE);
  });

  test('return page ' + PageConstant.SETTINGS + ' from /settings', () => {
    const { page } = handleUri({ uri: '/settings', actionType: 'DEEPLINK' });

    expect(page).toBe(PageConstant.SETTINGS);
  });

  test(
    'return page ' +
      PageConstant.SETTINGS_NOTIFICATIONS +
      ' from /settings/notifications',
    () => {
      const { page } = handleUri({
        uri: '/settings/notifications',
        actionType: 'DEEPLINK'
      });

      expect(page).toBe(PageConstant.SETTINGS_NOTIFICATIONS);
    }
  );

  test('return page ' + PageConstant.SUMMARY + ' from /summaries/20210', () => {
    const { page, dataId } = handleUri({
      uri: '/summaries/20210',
      actionType: 'DEEPLINK'
    });

    expect(page).toBe(PageConstant.SUMMARY);
    expect(dataId).toBe(20210);
  });

  test(
    'return page ' +
      PageConstant.SUMMARY_SOURCE +
      ' from /summary-sources/20210',
    () => {
      const { page, dataId } = handleUri({
        uri: '/summary-sources/20210',
        actionType: 'DEEPLINK'
      });

      expect(page).toBe(PageConstant.SUMMARY_SOURCE);
      expect(dataId).toBe(20210);
    }
  );

  test('return page ' + PageConstant.CHANNEL + ' from /channels/1050', () => {
    const { page, dataId } = handleUri({
      uri: '/channels/1050',
      actionType: 'DEEPLINK'
    });

    expect(page).toBe(PageConstant.CHANNEL);
    expect(dataId).toBe(1050);
  });

  test('return page ' + PageConstant.PORTLET + ' from /portlets/489', () => {
    const { page, dataId } = handleUri({
      uri: '/portlets/489',
      actionType: 'DEEPLINK'
    });

    expect(page).toBe(PageConstant.PORTLET);
    expect(dataId).toBe(489);
  });

  test('return page ' + PageConstant.AUDIOPLAYER + ' from /audioplayer', () => {
    const { page } = handleUri({
      uri: '/audioplayer',
      actionType: 'DEEPLINK'
    });

    expect(page).toBe(PageConstant.AUDIOPLAYER);
  });

  test('return page ' + PageConstant.PLAYLIST + ' from /playlist', () => {
    const { page } = handleUri({
      uri: '/playlist',
      actionType: 'DEEPLINK'
    });

    expect(page).toBe(PageConstant.PLAYLIST);
  });

  test(
    'return page ' + PageConstant.PLAYLIST_HISTORY + ' from /playlist/history',
    () => {
      const { page } = handleUri({
        uri: '/playlist/history',
        actionType: 'DEEPLINK'
      });

      expect(page).toBe(PageConstant.PLAYLIST_HISTORY);
    }
  );

  test('return page ' + PageConstant.UPGRADE + ' from /upgrade', () => {
    const { page } = handleUri({
      uri: '/upgrade',
      actionType: 'DEEPLINK'
    });

    expect(page).toBe(PageConstant.UPGRADE);
  });

  test(
    'return page ' + PageConstant.SUBSCRIPTION + ' from /subscriptions',
    () => {
      const { page } = handleUri({
        uri: '/subscriptions',
        actionType: 'DEEPLINK'
      });

      expect(page).toBe(PageConstant.SUBSCRIPTION);
    }
  );
});
